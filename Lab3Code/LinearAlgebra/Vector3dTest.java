//Minh Duc Hoang
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class Vector3dTest {
    
    @Test
    void constructorTest(){
        Vector3d v1 = new Vector3d(1, 2, 3);
        assertEquals(1, v1.getX());
        assertEquals(2, v1.getY());
        assertEquals(3, v1.getZ());
    }
    
    @Test
    void magnitudeTest(){
        Vector3d v2 = new Vector3d(1, 2, 3);
        assertEquals(Math.sqrt(14), v2.magnitude());
    }

    @Test
    void dotProductTest(){
        Vector3d v3 = new Vector3d(1, 1, 2);
        Vector3d v4 = new Vector3d(2, 3, 4);
        assertEquals(13, v3.dotProduct(v4));
    }

    @Test
    void addTest(){
        Vector3d v5 = new Vector3d(1, 1, 2);
        Vector3d v6 = new Vector3d(2, 3, 4);
        Vector3d v7 = new Vector3d(3, 4, 6);
        assertEquals(v7.getX(), v5.add(v6).getX());
        assertEquals(v7.getY(), v5.add(v6).getY());
        assertEquals(v7.getZ(), v5.add(v6).getZ());
    }

}
